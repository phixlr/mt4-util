//+------------------------------------------------------------------+
//|                                                      LowPass.mq4 |
//|                                                      Jarryd Beck |
//|                                                              foo |
//+------------------------------------------------------------------+
#property copyright "Jarryd Beck"
#property link      "foo"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 6
#property indicator_plots 1

double TrendLine[];
double Trigger[];
double Median[];

input double Alpha = 0.07;

int OnInit()
{
   SetIndexBuffer(0, TrendLine);
   SetIndexBuffer(1, Trigger);
   SetIndexBuffer(2, Median);
   
   SetIndexStyle(2, DRAW_NONE);
   SetIndexLabel(2, NULL);
   
   SetIndexLabel(1, "Trigger");
   
   SetIndexStyle(0, DRAW_LINE, EMPTY, EMPTY, clrRed);
   SetIndexStyle(1, DRAW_LINE, EMPTY, EMPTY, clrBlue);
   
   return(INIT_SUCCEEDED);
}

void start() {
  int counted = IndicatorCounted();
  int i = Bars - counted - 1;
  
  while (i >= 0) {
    Median[i] = (High[i] + Low[i])/2;
    
    if (Bars - i < 3) {
      TrendLine[i] = Median[i];
      Trigger[i] = Median[i];
    } else {
      TrendLine[i] = (Alpha - Alpha*Alpha/4)*Median[i] + 0.5*Alpha*Alpha*Median[i+1]
        - (Alpha - 0.75*Alpha*Alpha)*Median[i+2] + 2*(1-Alpha)*TrendLine[i+1]
        - (1-Alpha)*(1-Alpha)*TrendLine[i+2];
      Trigger[i] = 2*TrendLine[i] - TrendLine[i+2];
    }
    
    --i;
  }
}