// Calculates position size for you

// Just construct a PositionSize object:
//   position = new PositionSize(RiskFraction, AccountCurrency(), Symbol());
// and then call 
//   position.position(pipsRisk);

// Assumes quotes are in pipettes.

#property copyright "Jarryd Beck"
#property strict

#import "PositionSize.ex4"
  bool isValid(string);
#import

class Converter {
  public:
  virtual double convert(double) = 0;
};

class ConvertIdentity : public Converter {
  public:
  double convert(double a) {
    return a;
  }
};

class ConvertInvert : public Converter {
  public:
  double convert(double a) {
    return 1/a;
  }
};

class ExchangeRate {
  public:
  virtual double call() = 0;
};

class Exchanger : public ExchangeRate {
  public:
  Exchanger(string symbol) {
    m_symbol = symbol;
  }
  
  double call() {
    return SymbolInfoDouble(m_symbol, SYMBOL_ASK);
  }
  
  private:
  string m_symbol;
};

class SymbolInfoExchanger : public ExchangeRate {
  public:
  double call() {
    double pipPerLot = MarketInfo(Symbol(), MODE_TICKVALUE) /
      MarketInfo(Symbol(), MODE_TICKSIZE);
    
    return 100000 / pipPerLot;
  }
};

class FixedExchange : public ExchangeRate {
  public:
  FixedExchange(double rate) {
    m_rate = rate;
  }
  
  double call() {
    return m_rate;
  }
  
  private:
  double m_rate;
};

class PositionSize {
  public:
  PositionSize(double risk, string currency, string symbol)
  : m_exchange(NULL) {
    m_risk = risk;
    m_account = currency;
    m_symbol = symbol;
    m_converter = NULL;
    
    m_exchange = new SymbolInfoExchanger();
    m_converter = new ConvertIdentity();
    
    if (MarketInfo(Symbol(), MODE_TICKVALUE) == 0) {
      Print("Warning: don't seem to have conversion rate for ", Symbol());
    }
  }
  
  // Special constructor that takes a predefined conversion rate
  PositionSize(double risk, double conversion) {
    m_risk = risk;
    m_exchange = new FixedExchange(conversion);
    m_converter = new ConvertIdentity;
  }
  
  ~PositionSize() {
    delete m_converter;
    delete m_exchange;
  }
  
  double position(double stop) {
    double account_risk = AccountFreeMargin() * m_risk;
    double exchange = m_exchange.call();
    double spend = m_converter.convert(exchange) * account_risk;
    double units = spend/stop;
    return fmax(0.01, NormalizeDouble(units/100000, 2));
  }
  
  bool valid() {
    return m_exchange != NULL;
  }
  
  private:
  
  double m_risk;
  string m_account;
  string m_symbol;
  string m_exchangeSymbol;
  
  Converter* m_converter;
  ExchangeRate* m_exchange;
};